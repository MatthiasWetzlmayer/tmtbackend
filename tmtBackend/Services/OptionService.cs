﻿using System;
using System.Collections.Generic;
using System.Linq;
using tmtBackend.Dto;
using tmtBackendDb;

namespace tmtBackend.Controllers
{
    public class OptionService
    {
        private readonly tmtContext db;

        public OptionService(tmtContext db)
        {
            this.db = db;
        }

        public List<Settings> GetOptionsByUserId(int userId)
        {
            return db.Settings
                .Where(x => x.UserId == userId)
                .ToList();
        }

        public Settings AddOptionWithUserId(Settings settings)
        {
            db.Settings.Add(settings);
            db.SaveChanges();
            return settings;
        }

        public Settings EditOptionWithUserId(int id, int userId, Settings settings)
        {
            var original = db.Settings
                .FirstOrDefault(x => x.Id == id && x.UserId == userId);
            if (original == null) return null;
            original.SettingsName = settings.SettingsName;
            original.SettingsValue = settings.SettingsValue;

            db.SaveChanges();
            return original;
        }
    }
}
