using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class IssuesService : IIssuesService
    {
        private IGitlabService _gitlabService;

        public IssuesService(IGitlabService gitService)
        {
            this._gitlabService = gitService;
        }

        public async Task<IEnumerable<IssueResource>> GetProjectIssues(int id, UserInformation u)
        {
            int accesLevel = await _gitlabService.GetAccessLevelOfUserForProject(u.GitlabToken, id);

            List<GitlabIssue> issues = (await _gitlabService.GetIssues(u.GitlabToken, id)).Where(x => x.Assignee?.Id == int.Parse(u.UserId) || accesLevel > 39).ToList();
            //List<GitlabIssue> issues = _gitlabService.GetIssues(u.GitlabToken, id);
            return issues.Select(x => ConvertGitlabIssuesToIssueResource(x))
                .ToList();
        }

        private IssueResource ConvertGitlabIssuesToIssueResource(GitlabIssue issue)
        {
            return new IssueResource
            {
                Id = issue.Id,
                Name = issue.Title,
                Labels = issue.Labels,
                Assignee = issue.Assignee.Name,
                AssigneeId = issue.Assignee.Id,
                AuthorId = issue.Author.Id,
                EstimatedHours = issue.Time_stats.Time_estimate / 3600,
                SpentHours = issue.Time_stats.Total_time_spent / 3600,
                DueDate = issue.Due_date,
                ClosedAt = issue.Closed_at,
                Author = issue.Author.Name,
                CreatedAt = issue.Created_at
            };
        }

        public async Task<IssueResource> GetSingleProjectIssue(int projectId, int issueId, UserInformation u)
        {
            GitlabIssue gitlabIssue = (await _gitlabService.GetIssues(u.GitlabToken, projectId)).Where(x => x.Id == issueId).FirstOrDefault();
            return ConvertGitlabIssuesToIssueResource(gitlabIssue);
        }
    }
}
