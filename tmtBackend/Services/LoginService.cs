﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackendDb;

namespace tmtBackend.Services
{
    public class LoginService : ILoginService
    {
        private IGitlabService gitService;
        private readonly AppSettings appSettings;
        private tmtContext db;

        public LoginService(IGitlabService gitService, IOptions<AppSettings> appSettings, tmtContext db)
        {
            this.gitService = gitService;
            this.appSettings = appSettings.Value;
            this.db = db;
        }

        //private LoginService(GitlabService gitService, IOptions<AppSettings> appSettings)
        //{
        //    this.gitService = gitService;
        //    this.appSettings = appSettings.Value;
        //}
        public async Task<TokenResource> GetToken(LoginDTO login)
        {
            var gitToken = await gitService.Login(login);
            var gitUserId = await gitService.GetUserIdByToken(gitToken);


            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    //new Claim(ClaimTypes.NameIdentifier, gitUserId)
                    new Claim(ClaimTypes.NameIdentifier, gitUserId.ToString()),
                    new Claim(ClaimTypes.Authentication, gitToken),
                }),
                Expires = DateTime.UtcNow.AddHours(4),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            //CheckUserTable(gitUserId, login.Username);

            return new TokenResource { Token = tokenString };
        }

        private void CheckUserTable(int userId, string name)
        {
            var user = db.User.Where(x => x.Id == userId)
                .FirstOrDefault();

            if(user == null)
            {
                db.User.Add(new User
                {
                    Id = userId,
                    Name = name,
                    Email = ""
                }); ;
                db.SaveChanges();
            }
        }
    }
}
