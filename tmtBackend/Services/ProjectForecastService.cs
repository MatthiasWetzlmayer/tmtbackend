using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Helpers;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class ProjectForecastService : IProjectForecastService
    {
        private IGitlabService service;
        public ProjectForecastService(IGitlabService service)
        {
            this.service = service;
        }

        private List<WorkReportDataDTO> InitList(List<string> names)
        {
            var list = new List<WorkReportDataDTO>();

            foreach (var n in names)
            {
                list.Add(new WorkReportDataDTO
                {
                    Name = n,
                    Data = new List<string>()
                });
            }

            return list;
        }

        public async Task<IEnumerable<WorkReportDataDTO>> calculateYCoordinates(int id, UserInformation u, FilterDTO filter)
        {
            var list = InitList(new List<string>(new string[] { "Dates", "Estimate", "Spend" }));


            var gitlabIssues = await service.GetIssues(u.GitlabToken, id);

            var issues = gitlabIssues.FilterIssues(filter).Select(x => ConvertGitlabIssuesToIssueResource(x)).ToList();

            var dates = issues
                .Where(x => x.DueDate != null)
                .OrderBy(x => DateTime.ParseExact(x.DueDate, "yyyy-MM-dd", null))
                .GroupBy(x => x.DueDate)
                .Select(x => x.Key)
                .ToList();

            var lastDate = DateTime.Parse(dates[dates.Count() - 1]);
            var firstDate = DateTime.Parse(dates[0]);

            var totalEstimate = issues.Select(x => x.EstimatedHours).Sum();

            var timeBetween = lastDate - firstDate;

            var estimatePerDay = totalEstimate / timeBetween.Days;

            for (int i = 0; i < timeBetween.Days; i++)
            {
                list[1].Data.Add((totalEstimate - estimatePerDay * i).ToString());
            }


            // to get the spend graph, we calculated the .spenthours property of the issues, which are not closed
            var firstCreatedAt = issues
                .Select(x => DateTime.Parse(x.CreatedAt))
                .OrderBy(x => x)
                .ToList()[0];

            DateTime date = firstCreatedAt;
            date = date.AddHours(23 - date.Hour);
            date = date.AddMinutes(59 - date.Minute);
            date = date.AddSeconds(59 - date.Second);
            for (int i = 0; i < list[1].Data.Count(); i++)
            {
                date = date.AddDays(1);
                double totalSpent = 0;

                list[0].Data.Add(date.ToString("yyyy-MM-dd"));

                try
                {
                    totalSpent = issues
                        .Where(x => x.ClosedAt != null)
                        .Where(x =>  DateTime.Parse(x.ClosedAt) < date)
                        .Select(x => x.SpentHours)
                        .Sum();
                }catch(Exception e)
                {
                    totalSpent = 0;
                }

                list[2].Data.Add((totalEstimate - totalSpent).ToString());
            }


            return list;

        }

        private DateTime getStartDate(List<IssueResource> issues)
        {
            var smallest = issues.Min(a => DateTime.Parse(a.DueDate));
            return smallest;
        }

        private IssueResource ConvertGitlabIssuesToIssueResource(GitlabIssue issue)
        {
            return new IssueResource
            {
                Id = issue.Id,
                Name = issue.Title,
                Labels = issue.Labels,
                Assignee = issue.Assignee?.Name ?? "",
                AssigneeId = issue.Assignee?.Id ?? -1,
                AuthorId = issue.Author.Id,
                EstimatedHours = issue.Time_stats.Time_estimate / 3600,
                SpentHours = issue.Time_stats.Total_time_spent / 3600,
                DueDate = issue.Due_date,
                ClosedAt = issue.Closed_at,
                Author = issue.Author.Name,
                CreatedAt = issue.Created_at
            };
        }
    }
}