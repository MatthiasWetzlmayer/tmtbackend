﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Server.IIS;
using tmtBackend.Exceptions;
using tmtBackend.Interfaces;

namespace Testing
{
  public class RestService : IRestService
  {
    //private static readonly HttpClient client;

    public RestService()
    {
      //client = new HttpClient();
    }

    //public async Task<T> Get<T>(string path)
    //{
    //  try
    //  {
    //    HttpResponseMessage response = await client.GetAsync(path);
    //    response.EnsureSuccessStatusCode();
    //    string responseBody = await response.Content.ReadAsStringAsync();
    //    T deserialized = JsonConvert.DeserializeObject<T>(responseBody);
    //    return deserialized;
    //  }
    //  catch (HttpRequestException e)
    //  {
    //    Console.WriteLine("\nException Caught! (FetchNext)");
    //    Console.WriteLine("Message :{0} ", e.Message);
    //    return default(T);
    //  }
    //}
    //public async Task<T> Post<T, B>(string path, Dictionary<string, string> headers, B body)
    //{
    //  try
    //  {
    //    var json = JsonConvert.SerializeObject(body);
    //    var data = new StringContent(json, Encoding.UTF8, "application/json");
    //    HttpResponseMessage response = await client.PostAsync(path, data);
    //    response.EnsureSuccessStatusCode();
    //    string responseBody = await response.Content.ReadAsStringAsync();
    //    T deserialized = JsonConvert.DeserializeObject<T>(responseBody);
    //    return deserialized;
    //  }
    //  catch (HttpRequestException e)
    //  {
    //    Console.WriteLine("\nException Caught! (Finished)");
    //    Console.WriteLine("Message :{0} ", e.Message);
    //    return default(T);
    //  }
    //}

    //public async Task<T> Put<T, B>(string path, Dictionary<string, string> headers, B body)
    //{
    //  try
    //  {
    //    var json = JsonConvert.SerializeObject(body);
    //    var data = new StringContent(json, Encoding.UTF8, "application/json");
    //    HttpResponseMessage response = await client.PutAsync(path, data);
    //    response.EnsureSuccessStatusCode();
    //    string responseBody = await response.Content.ReadAsStringAsync();
    //    T deserialized = JsonConvert.DeserializeObject<T>(responseBody);
    //    return deserialized;
    //  }
    //  catch (HttpRequestException e)
    //  {
    //    Console.WriteLine("\nException Caught! (Finished)");
    //    Console.WriteLine("Message :{0} ", e.Message);
    //    return default(T);
    //  }
    //}


    //public async Task<T> Delete<T>(string path, Dictionary<string, string> headers)
    //{
    //  try
    //  {

    //    HttpResponseMessage response = await client.DeleteAsync(path);
    //    response.EnsureSuccessStatusCode();
    //    string responseBody = await response.Content.ReadAsStringAsync();
    //    T deserialized = JsonConvert.DeserializeObject<T>(responseBody);
    //    return deserialized;
    //  }
    //  catch (HttpRequestException e)
    //  {
    //    Console.WriteLine("\nException Caught! (Finished)");
    //    Console.WriteLine("Message :{0} ", e.Message);
    //    return default(T);
    //  }
    //}

    private void CheckResponse(HttpResponseMessage response)
    {
      if ((int)response.StatusCode >= 400 && (int)response.StatusCode < 500)
      {
        throw new ResponseException
        {
          StatusCode = (int)response.StatusCode,
          Message = response.ReasonPhrase,
        };
      }
    }

    public async Task<T> Get<T>(string path, Dictionary<string, string> headers = null)
    {
      using (var client = new HttpClient())
      {
        if (headers != null)
        {
          foreach (var header in headers)
          {
            client.DefaultRequestHeaders.Add(header.Key, header.Value);
          }
        }
        using (var response = await client.GetAsync(path))
        {
          string responseData = await response.Content.ReadAsStringAsync();
          // Console.WriteLine(responseData);

          CheckResponse(response);

          T deserialized = JsonConvert.DeserializeObject<T>(responseData);
          return deserialized;
        }
      }
    }

    public async Task<T> Post<T, B>(string path, Dictionary<string, string> headers, B body)
    {
      using (var client = new HttpClient())
      {
        if (headers != null)
        {
          foreach (var header in headers)
          {
            client.DefaultRequestHeaders.Add(header.Key, header.Value);
          }
        }
        var json = JsonConvert.SerializeObject(body);
        var data = new StringContent(json, Encoding.UTF8, "application/json");
        using (var response = await client.PostAsync(path, data))
        {
          string responseData = await response.Content.ReadAsStringAsync();
          // Console.WriteLine(responseData);

          CheckResponse(response);

          T deserialized = JsonConvert.DeserializeObject<T>(responseData);
          return deserialized;
        }
      }
    }

    public async Task<T> Put<T, B>(string path, Dictionary<string, string> headers, B body)
    {
      using (var client = new HttpClient())
      {
        if (headers != null)
        {
          foreach (var header in headers)
          {
            client.DefaultRequestHeaders.Add(header.Key, header.Value);
          }
        }
        var json = JsonConvert.SerializeObject(body);
        var data = new StringContent(json, Encoding.UTF8, "application/json");
        using (var response = await client.PutAsync(path, data))
        {
          string responseData = await response.Content.ReadAsStringAsync();
          // Console.WriteLine(responseData);

          CheckResponse(response);

          T deserialized = JsonConvert.DeserializeObject<T>(responseData);
          return deserialized;
        }
      }
    }

    public async Task<T> Delete<T>(string path, Dictionary<string, string> headers)
    {
      using (var client = new HttpClient())
      {
        if (headers != null)
        {
          foreach (var header in headers)
          {
            client.DefaultRequestHeaders.Add(header.Key, header.Value);
          }
        }
        using (var response = await client.DeleteAsync(path))
        {
          string responseData = await response.Content.ReadAsStringAsync();
          // Console.WriteLine(responseData);

          CheckResponse(response);

          T deserialized = JsonConvert.DeserializeObject<T>(responseData);
          return deserialized;
        }
      }
    }
  }
}
