using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class GitlabService : IGitlabService
    {
        private IRestService rest;
        private string baseUrl = "";

        public GitlabService(IRestService rest, IConfiguration settings)
        {
            this.rest = rest;
            this.baseUrl = settings.GetSection("AppSettings")["GitlabURL"];
            Console.WriteLine($"GitlabService::GitlabService Using URL: {baseUrl}");
        }

        public async Task<int> GetAccessLevelOfUserForProject(string token, int projectId)
        {
            Console.WriteLine($"GitlabService::GetAccessLevelOfUserForProject Token: {token} projectId: {projectId}");
            //var member = rest.Get<GitlabMember>($"{baseUrl}/api/v4/projects/{projectId}/members/{userId}", GetAuthorizationDictionary(token));
            //return member.Result.Access_level;

            var project = await rest.Get<GitlabProjects>($"{baseUrl}/api/v4/projects/{projectId}", GetAuthorizationDictionary(token));
            int aLevel = 0;
            if (project.Permissions.Project_access != null)
                aLevel = project.Permissions.Project_access.Access_level;
            else if (project.Permissions.Group_access != null)
                aLevel = project.Permissions.Group_access.Access_level;

            //int aLevel = project.Result.Permissions.Project_access.Access_level;
            return aLevel;
        }

        public async Task<IEnumerable<GitlabMember>> GetMembersOfProject(string token, int projectId)
        {
            var members = await rest.Get<List<GitlabMember>>($"{baseUrl}/api/v4/projects/{projectId}/members", GetAuthorizationDictionary(token));
            return members;
        }

        public async Task<IEnumerable<GitlabLabel>> GetLabelsOfProject(string token, int projectId)
        {
            var labels = await rest.Get<List<GitlabLabel>>($"{baseUrl}/api/v4/projects/{projectId}/labels", GetAuthorizationDictionary(token));
            return labels;
        }


        public async Task<List<GitlabIssue>> GetIssues(string token, int projectId)
        {
            Console.WriteLine($"GitlabService::GetIssues Token: {token} projectId: {projectId}");
            List<GitlabIssue> gitlabIssues = new List<GitlabIssue>();
            int page = 1;
            List<GitlabIssue> currentPage = new List<GitlabIssue>();
            do
            {
                currentPage = await rest.Get<List<GitlabIssue>>($"{baseUrl}/api/v4/projects/{projectId}/issues?per_page=100&page={page}", GetAuthorizationDictionary(token));
                gitlabIssues.AddRange(currentPage);
                page++;
            } while (currentPage.Count() % 100 == 0 && currentPage.Count() != 0);

            //var issues = gitlabIssues.Result.Select(x => new Issue
            //{
            //    Assignee = x.Assignee,
            //    Description = x.Description,
            //    Id = x.Id,
            //    Labels = x.Labels,
            //    Project_id = x.Project_id,
            //    State = x.State,
            //    Time_stats = x.Time_stats,
            //    Title = x.Title,
            //    Due_date = x.Due_date
            //}).ToList();
            Console.WriteLine($"GitlabService::GetIssues IssuesCount: {gitlabIssues.Count()}");
            return gitlabIssues;
        }


        public async Task<List<GitlabProjects>> GetProjects(string token)
        {
            Console.WriteLine($"GitlabService::GetProjects Token:{token}");
            List<GitlabProjects> gitlabProjects = new List<GitlabProjects>();
            List<GitlabProjects> currentPage = new List<GitlabProjects>();

            do
            {
                currentPage = await rest.Get<List<GitlabProjects>>(
                    $"{baseUrl}/api/v4/projects?membership=true&per_page=100", GetAuthorizationDictionary(token));
                gitlabProjects.AddRange(currentPage);

            } while (currentPage.Count() % 100 == 0 && currentPage.Count() != 0);

            Console.WriteLine($"GitlabService::GetProjects ProjectsCount: {gitlabProjects.Count()}");
            return gitlabProjects;
        }

        public async Task<int> GetUserIdByToken(string token)
        {
            Console.WriteLine($"GitlabService::GetUserIdByToken Token:{token}");
            var tokenInfo = await rest.Get<GitlabTokenInformation>($"{baseUrl}/oauth/token/info?access_token={token}", new Dictionary<string, string>());
            return tokenInfo.Resource_owner_id;
        }

        public async Task<string> Login(LoginDTO login)
        {
            Console.WriteLine($"GitlabService::Login username:{login.Username}");
            var client = new RestClient($"{baseUrl}/oauth/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            client.Authenticator = new HttpBasicAuthenticator("1947b1ffd95754b6ad47fda83967913597c0b0a2d16808902ce7adf52bd83909", "a94914e3e8c5fcd0cec3373da902f14f89e8c8dc9b069a7e7a198fcce94bbedd");
            //request.AddHeader("Authorization", $"Basic MTk0N2IxZmZkOTU3NTRiNmFkNDdmZGE4Mzk2NzkxMzU5N2MwYjBhMmQxNjgwODkwMmNlN2FkZjUyYmQ4MzkwOTphOTQ5MTRlM2U4YzVmY2QwY2VjMzM3M2RhOTAyZjE0Zjg5ZThjOGRjOWIwNjlhN2U3YTE5OGZjY2U5NGJiZWRk");
            request.AddHeader("Content-Type", $"application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", login.Username);
            request.AddParameter("password", login.Password);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            var data = JsonConvert.DeserializeObject<GitlabTokenData>(response.Content);
            return data.Access_token;

            //Console.WriteLine($"GitlabService::Login username:{login.Username}");
            //var values = new Dictionary<string, string>
            //{
            //    {"grant_type", "password" },
            //    {"username", login.Username },
            //    {"password", login.Password }
            //};
            //var content = new FormUrlEncodedContent(values);

            ////var response = await client.PostAsync("https://gitlab.com/oauth/token", content);
            //var token = await rest.Post<GitlabLoginResponse, Dictionary<string, string>>($"{baseUrl}/oauth/token", new Dictionary<string, string>(), values);
            //Console.WriteLine($"Gitlab token: {token.Access_token}");
            //return token.Access_token;
        }

        private Dictionary<string, string> GetAuthorizationDictionary(string value)
        {
            return new Dictionary<string, string> { { "Authorization", $"Bearer {value}" } };
        }


    }
}
