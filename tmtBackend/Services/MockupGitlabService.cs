﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class MockupGitlabService : IGitlabService
    {
        #region Data

        

        #endregion

        public int GetAccessLevelOfUserForProject(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        public List<GitlabIssue> GetIssues(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GitlabLabel>> GetLabelsOfProject(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GitlabMember>> GetMembersOfProject(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        public List<GitlabProjects> GetProjects(string token)
        {
            throw new NotImplementedException();
        }

        public int GetUserIdByToken(string token)
        {
            throw new NotImplementedException();
        }

        public string Login(LoginDTO login)
        {
            throw new NotImplementedException();
        }

        Task<int> IGitlabService.GetAccessLevelOfUserForProject(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        Task<List<GitlabIssue>> IGitlabService.GetIssues(string token, int projectId)
        {
            throw new NotImplementedException();
        }

        Task<List<GitlabProjects>> IGitlabService.GetProjects(string token)
        {
            throw new NotImplementedException();
        }

        Task<int> IGitlabService.GetUserIdByToken(string token)
        {
            throw new NotImplementedException();
        }

        Task<string> IGitlabService.Login(LoginDTO login)
        {
            throw new NotImplementedException();
        }
    }
}
