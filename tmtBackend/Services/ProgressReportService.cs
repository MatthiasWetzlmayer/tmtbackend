﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class ProgressReportService : IProgressReportService
    {
        private IGitlabService gitlabService;

        public ProgressReportService(IGitlabService gitlabService)
        {
            this.gitlabService = gitlabService;
        }

        public async Task<List<IssueOfUserResource>> ConvertIssueToIssueUserResources(string token, int projectId, int userId)
        {
            int accesLevel = await gitlabService.GetAccessLevelOfUserForProject(token, projectId);

            List<GitlabIssue> issues = (await gitlabService.GetIssues(token, projectId))
                .Where(x => x.Assignee?.Id == userId || accesLevel > 39)
                .ToList();
            List<IssueOfUserResource> issueOfUsers = new List<IssueOfUserResource>();
            foreach (GitlabIssue issue in issues)
            {
                if (issue.Assignee != null)
                {
                    IssueOfUserResource issueOfUser = issueOfUsers.Find(x => x.UserId == issue.Assignee.Id);
                    if (issueOfUser == null)
                    {
                        issueOfUser = new IssueOfUserResource() { Username = issue.Assignee.Username, UserId = issue.Assignee.Id, Issues = new List<SmallIssueResource>() };
                        issueOfUsers.Add(issueOfUser);
                    }
                    issueOfUser.Issues.Add(ConvertIssueToSmallIssueResource(issue));
                }
            }
            return issueOfUsers;
        }

        public async Task<IssueOfUserResource> ConvertIssueToIssueUserResource(string token, int projectId, int userId, int myId)
        {
            int accesLevel = await gitlabService.GetAccessLevelOfUserForProject(token, projectId);

            List<GitlabIssue> issues = new List<GitlabIssue>();

            if (myId == userId)
            {
                issues = (await gitlabService.GetIssues(token, projectId))
                    .Where(x => x.Assignee?.Id == userId)
                    .ToList();
            }
            else
            {
                if(accesLevel > 39)
                {
                    issues = (await gitlabService.GetIssues(token, projectId))
                        .Where(x => x.Assignee?.Id == userId)
                        .ToList();
                }
                else
                {
                    throw new UnauthorizedAccessException();
                }
            }



            issues = issues.FindAll(x => x.Assignee?.Id == userId);
            List<SmallIssueResource> smallIssues = new List<SmallIssueResource>();


            foreach (GitlabIssue issue in issues)
            {
                smallIssues.Add(ConvertIssueToSmallIssueResource(issue));
            }
            IssueOfUserResource issueOfUser = new IssueOfUserResource() { UserId = userId, Username = issues.First().Assignee?.Username, Issues = smallIssues };
            return issueOfUser;
        }

        private SmallIssueResource ConvertIssueToSmallIssueResource(GitlabIssue issue)
        {
            SmallIssueResource smallIssue = new SmallIssueResource()
            {
                Id = issue.Id,
                Name = issue.Title,
                Labels = issue.Labels,
                Author = issue.Author.Name,
                ClosedAt = issue.Closed_at,
                DueDate = issue.Due_date,
                EstimatedHours = issue.Time_stats.Time_estimate,
                SpentHours = issue.Time_stats.Total_time_spent
            };
            return smallIssue;
        }
    }
}
