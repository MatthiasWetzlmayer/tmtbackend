﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class ProjectsService : IProjectsService
    {
        private IGitlabService gitService;

        public ProjectsService(IGitlabService gitService)
        {
            this.gitService = gitService;
        }

        public async Task<IEnumerable<Project>> GetProjects(string gitlabToken)
        {
            Console.WriteLine($"ProjectsService::GetProjects GitToken: {gitlabToken}");
            var gitProjects = await gitService.GetProjects(gitlabToken);

            var projects = gitProjects.Select(async x => new Project
            {
                Id = x.Id,
                AmountOfIssues = x.Open_issues_count,
                Creator = x.Owner?.Name,
                Name = x.Name,
                Role = await gitService.GetAccessLevelOfUserForProject(gitlabToken, x.Id)
            });

            return await Task.WhenAll(projects);
        }

        public async Task<IEnumerable<MemberDTO>> GetMembersOfProject(string gitlabToken, int projectId)
        {
            Console.WriteLine($"ProjectsService::GetMembersOfProject GitToken: {gitlabToken}, ProjectId: {projectId}");

            var members = await gitService.GetMembersOfProject(gitlabToken, projectId);

            return members.Select(x => new MemberDTO
            {
                Id = x.Id,
                AccessLevel = x.Access_level,
                Email = x.Email,
                Name = x.Name,
                State = x.State,
                Username = x.Username
            });
        }

        public async Task<IEnumerable<LabelDTO>> GetLabelsOfProject(string gitlabToken, int projectId)
        {
            Console.WriteLine($"ProjectsService::GetMembersOfProject GitToken: {gitlabToken}, ProjectId: {projectId}");

            var labels = await gitService.GetLabelsOfProject(gitlabToken, projectId);

            return labels.Select(x => new LabelDTO
            {
                Id = x.Id,
                Name = x.Name,
                Color = x.Color
            });
        }
    }
}
