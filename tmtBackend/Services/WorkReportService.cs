﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Services
{
    public class WorkReportService : IWorkReportService
    {
        private IGitlabService gitlabService;

        int dividend = 3600; // Timestats from gitlab are divided through this value

        public WorkReportService(IGitlabService gitlabService)
        {
            this.gitlabService = gitlabService;
        }

        public async Task<IEnumerable<WorkReportDataDTO>> GetWorkreportForUsers(string token, int projectId, int userId, FilterDTO filter)
        {

            int accesLevel = await gitlabService.GetAccessLevelOfUserForProject(token, projectId);

            var gitlabIssues = (await gitlabService.GetIssues(token, projectId))
                .Where(x => x.Assignee?.Id == userId || accesLevel > 39)
                .ToList();

            List<GitlabIssue> issues = gitlabIssues.FilterIssues(filter);

            List<IssueOfUserResource> issueOfUsers = ConvertIssuesToIssuesOfUser(issues);

            return ConvertIssuesOfUserToGraphData(issueOfUsers);
        }



        public async Task<IEnumerable<WorkReportDataDTO>> GetCompleteWorkreport(string token, int projectId, int userId, FilterDTO filter)
        {
            int accesLevel = await gitlabService.GetAccessLevelOfUserForProject(token, projectId);

            var gitlabIssues = (await gitlabService.GetIssues(token, projectId))
                .Where(x => x.Assignee?.Id == userId || accesLevel > 39)
                .ToList();

            List<GitlabIssue> issues = gitlabIssues.FilterIssues(filter);

            return ConvertGitlabIssuesToGraphData(issues);
        }

        private List<IssueOfUserResource> ConvertIssuesToIssuesOfUser(List<GitlabIssue> issues)
        {
            List<IssueOfUserResource> issueOfUsers = new List<IssueOfUserResource>();
            foreach (GitlabIssue issue in issues)
            {
                if (issue.Assignee != null)
                {
                    IssueOfUserResource issueOfUser = issueOfUsers.Find(x => x.UserId == issue.Assignee.Id);
                    if (issueOfUser == null)
                    {
                        issueOfUser = new IssueOfUserResource() { Username = issue.Assignee.Name, UserId = issue.Assignee.Id, Issues = new List<SmallIssueResource>() };
                        issueOfUsers.Add(issueOfUser);
                    }
                    issueOfUser.Issues.Add(ConvertIssueToSmallIssueResource(issue));
                }
            }

            return issueOfUsers;
        }



        private IEnumerable<WorkReportDataDTO> ConvertGitlabIssuesToGraphData(List<GitlabIssue> issues)
        {
            var list = InitList(new List<string>(new string[] { "Estimate", "EstimateFinished", "Spend", "SpendFinished" }));

            var data = issues.GetTimestatsOfIssues();


            list[0].Data.Add((data.TotalEstimate / dividend).ToString());
            list[1].Data.Add((data.TotalEstimateFinished / dividend).ToString());
            list[2].Data.Add((data.TotalSpend / dividend).ToString());
            list[3].Data.Add((data.TotalSpendFinished / dividend).ToString());


            return list;
        }

        private List<WorkReportDataDTO> InitList(List<string> names)
        {
            var list = new List<WorkReportDataDTO>();

            foreach (var n in names)
            {
                list.Add(new WorkReportDataDTO
                {
                    Name = n,
                    Data = new List<string>()
                });
            }

            return list;
        }

        private IEnumerable<WorkReportDataDTO> ConvertIssuesOfUserToGraphData(List<IssueOfUserResource> issueOfUsers)
        {
            var list = InitList(new List<string>(new string[] { "Usernames", "Ids", "Estimate", "EstimateFinished", "Spend", "SpendFinished" }));


            for (int i = 0; i < issueOfUsers.Count; i++)
            {
                var issueOfUser = issueOfUsers[i];
                list[0].Data.Add(issueOfUser.Username);

                list[1].Data.Add(issueOfUser.UserId.ToString());

                var data = issueOfUser.Issues.GetTimestatsOfIssues();

                list[2].Data.Add((data.TotalEstimate / dividend).ToString());
                list[3].Data.Add((data.TotalEstimateFinished / dividend).ToString());
                list[4].Data.Add((data.TotalSpend / dividend).ToString());
                list[5].Data.Add((data.TotalSpendFinished / dividend).ToString());
            }


            return list;
        }


        private SmallIssueResource ConvertIssueToSmallIssueResource(GitlabIssue issue)
        {
            SmallIssueResource smallIssue = new SmallIssueResource()
            {
                Id = issue.Id,
                Name = issue.Title,
                Labels = issue.Labels,
                Author = issue.Author.Name,
                ClosedAt = issue.Closed_at,
                DueDate = issue.Due_date,
                EstimatedHours = issue.Time_stats.Time_estimate,
                SpentHours = issue.Time_stats.Total_time_spent
            };
            return smallIssue;
        }
    }
}
