﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabLabel
    {
        //       {
        //  "id" : 1,
        //  "name" : "bug",
        //  "color" : "#d9534f",
        //  "text_color" : "#FFFFFF",
        //  "description": "Bug reported by user",
        //  "description_html": "Bug reported by user",
        //  "open_issues_count": 1,
        //  "closed_issues_count": 0,
        //  "open_merge_requests_count": 1,
        //  "subscribed": false,
        //  "priority": 10,
        //  "is_project_label": true
        //},

        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

    }
}
