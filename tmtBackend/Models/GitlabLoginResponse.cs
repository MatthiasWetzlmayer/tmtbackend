﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabLoginResponse
    {
        public string Access_token { get; set; }
        public string Token_type { get; set; }

    }
}
