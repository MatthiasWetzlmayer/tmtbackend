﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabProjects
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Open_issues_count { get; set; }
        public GitlabOwner Owner { get; set; }

        public GitlabProjectAcess Permissions { get; set; }

        //"permissions": {
        //"project_access": {
        //    "access_level": 40,
        //    "notification_level": 3
        //},
        //"group_access": null
    }
}
