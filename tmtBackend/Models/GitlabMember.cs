﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabMember
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public int Access_level { get; set; }
        public string Email { get; set; }

    }
}
