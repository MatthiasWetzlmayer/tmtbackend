﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class WorkReportDataDTO
    {
        public string Name { get; set; }
        public List<string> Data { get; set; }
    }
}
