﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class FilterDTO
    {
        public List<string> Labels { get; set; }
        public string TimePeriod { get; set; }

    }
}
