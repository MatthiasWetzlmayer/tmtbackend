﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GraphDataTimeStats
    {
        public double TotalEstimate { get; set; }
        public double TotalEstimateFinished { get; set; }
        public double TotalSpend { get; set; }
        public double TotalSpendFinished { get; set; }
    }
}
