﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabIssue
    {

        // https://docs.gitlab.com/ee/api/issues.html
        // Unter Project Issues
        public int Id { get; set; }
        public string Description { get; set; }
        public int Project_id { get; set; }
        public string State { get; set; }
        public Assignee Assignee { get; set; }
        public List<string> Labels { get; set; }
        public string Title { get; set; }
        public TimeStats Time_stats { get; set; }
        public string Due_date { get; set; }
        public string Closed_at { get; set; }
        public GitlabOwner Author  { get; set; }

        public string Created_at { get; set; }

    }
}
