﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class GitlabProjectAcess
    {
        //"permissions": {
        //"project_access": {
        //    "access_level": 40,
        //    "notification_level": 3
        //},
        //"group_access": null

        public GitlabAccessLevel Project_access { get; set; }
        public GitlabAccessLevel Group_access { get; set; }
    }
}
