﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class UserInformation
    {
        public string UserId { get; set; }
        public string GitlabToken { get; set; }

    }
}
