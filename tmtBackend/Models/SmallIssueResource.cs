﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class SmallIssueResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Labels { get; set; }
        //public List<string> users { get; set; }
        public double EstimatedHours { get; set; }
        public double SpentHours { get; set; }
        public string DueDate { get; set; }
        public string ClosedAt { get; set; }
        public string Author { get; set; }

    }
}
