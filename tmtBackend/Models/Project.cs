﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public int Role { get; set; }
        public int AmountOfIssues { get; set; }
    }
}
