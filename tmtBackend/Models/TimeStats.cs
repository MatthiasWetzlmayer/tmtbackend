﻿namespace tmtBackend.Models
{
    public class TimeStats
    {
        public int Time_estimate { get; set; }
        public int Total_time_spent { get; set; }

    }
}