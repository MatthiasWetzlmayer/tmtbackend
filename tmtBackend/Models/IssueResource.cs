﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Models
{
    public class IssueResource
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<string> Labels { get; set; }

        //public string State { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public int AuthorId { get; set; }
        public string Assignee { get; set; }
        public int AssigneeId { get; set; }

        //public List<string> Users { get; set; }
        public double EstimatedHours { get; set; }
        public double SpentHours { get; set; }
        public string DueDate { get; set; }
        public string ClosedAt { get; set; }
        public string CreatedAt { get; set; }
    }
}
