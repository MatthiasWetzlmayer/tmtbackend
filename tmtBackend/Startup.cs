using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Testing;
using tmtBackend.Controllers;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;
using tmtBackendDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using tmtBackend.Exceptions;

namespace tmtBackend
{
    public class Startup
    {
        private const string swaggerVersion = "v1";
        private const string swaggerTitle = "tmtBackend";

        private readonly string myAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    x => x.AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .SetIsOriginAllowed(hosts => true)
                );
            });
            services.AddMvc(options => options.EnableEndpointRouting = false);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerVersion, new OpenApiInfo
                {
                    Title = swaggerTitle,
                    Version = swaggerVersion
                });
            });

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var conString = Configuration.GetConnectionString("SypDb");
            Console.WriteLine($"Startup::ConfigureServices: Connection string: {conString}");
            services.AddDbContext<tmtContext>(options => options.UseMySql(conString));

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);


            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            // Add Scoped Here!
            services.AddScoped<IRestService, RestService>();
            services.AddScoped<IGitlabService, GitlabService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<IIssuesService, IssuesService>();
            services.AddScoped<IProgressReportService, ProgressReportService>();
            services.AddScoped<IProjectForecastService, ProjectForecastService>();
            services.AddScoped<IWorkReportService, WorkReportService>();
            services.AddScoped<OptionService>();
    
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint($"/swagger/{swaggerVersion}/swagger.json", swaggerTitle); });

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseExceptionHandler(config =>
            {
                config.Run(async context =>
                {
                    // Use the information about the exception 
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";
                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null)
                    {
                        if (error.Error is ResponseException ex)
                        {
                            context.Response.StatusCode = ex.StatusCode;
                            await context.Response.WriteAsync(
                                $"Exception: {ex.Message}");
                        }
                        else
                        {
                            await context.Response.WriteAsync(
                                $"Exception: {error.Error.Message} {error.Error?.InnerException.Message}");
                        }
                    }
                });
            });

            app.UseMvc();
        }
    }
}