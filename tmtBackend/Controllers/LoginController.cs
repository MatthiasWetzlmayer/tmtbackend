﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService service;
        public LoginController(ILoginService service)
        {
            this.service = service;
        }
        // POST: /Login
        [AllowAnonymous]
        [HttpPost]
        public async Task<TokenResource> Login([FromBody] LoginDTO login)
        {
            Console.WriteLine($"LoginController::Login Username: {login.Username}");
            return await service.GetToken(login);
        }
    }
}
