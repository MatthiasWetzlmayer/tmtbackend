﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Controllers
{
    [Route("projects")]
    [ApiController]
    [Authorize]
    public class WorkReportController : ControllerBase
    {
        private IWorkReportService service;
        public WorkReportController(IWorkReportService service)
        {
            this.service = service;
        }

        [HttpGet("{id}/workreport")]
        public async Task<IEnumerable<WorkReportDataDTO>> GetWorkreportForUsers(int id, [FromQuery] string label, [FromQuery] string time)
        {
            UserInformation u = this.GetAuthUserData();
            Console.WriteLine($"ProjectsController::GetProjects UserId: {u.UserId}");
            var filter = new FilterDTO
            {
                Labels = label == null ? new List<string>() :  new List<string>(new string[] { label }),
                TimePeriod = time
            };
            return await service.GetWorkreportForUsers(u.GitlabToken, id, int.Parse(u.UserId), filter);
        }

        [HttpGet("{id}/workreport/all")]
        public async Task<IEnumerable<WorkReportDataDTO>> GetCompleteWorkreport(int id, [FromQuery] string label, [FromQuery] string time)
        {
            UserInformation u = this.GetAuthUserData();
            Console.WriteLine($"ProjectsController::GetProjects UserId: {u.UserId}");
            var filter = new FilterDTO
            {
                Labels = label == null ? new List<string>() : new List<string>(new string[] { label }),
                TimePeriod = time
            };
            return await service.GetCompleteWorkreport(u.GitlabToken, id, int.Parse(u.UserId), filter);
        }

    }
}
