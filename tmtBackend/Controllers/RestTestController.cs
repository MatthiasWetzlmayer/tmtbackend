﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class RestTestController : ControllerBase
    {
        public RestTestController()
        {
            Console.WriteLine("testline");
        }

        [AllowAnonymous]
        [HttpGet("GetTest")]
        public bool GetTest(int id)
        {
            return true;
        }

        [HttpPost("PostTest")]
        public bool PostTest()
        {
            return true;
        }

        [HttpPut("PutTest")]
        public bool PutTest()
        {
            return true;
        }

        [HttpDelete("DeleteTest")]
        public bool DeleteTest(string id)
        {
            return true;
        }
    }
}
