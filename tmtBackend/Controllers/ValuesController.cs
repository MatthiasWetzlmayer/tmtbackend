﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tmtBackend.Dto;
using tmtBackendDb;

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly tmtContext db;
        public ValuesController(tmtContext db)
        {
            this.db = db;
        }

        [HttpGet("GetUsers")]
        public object GetUsers()
        {
            return db.User.ToList().Select(x => new UserDto().CopyPropertiesFrom(x));
        }
    }
}
