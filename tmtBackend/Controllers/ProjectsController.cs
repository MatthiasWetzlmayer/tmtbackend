﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private IProjectsService service;
        public ProjectsController(IProjectsService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<Project>> GetProjects()
        {
            UserInformation u = this.GetAuthUserData();
            Console.WriteLine($"ProjectsController::GetProjects UserId: {u.UserId}");
            return await this.service.GetProjects(u.GitlabToken);
        }

        [HttpGet("{id}/members")]
        public async Task<IEnumerable<MemberDTO>> GetMembers(int id)
        {
            UserInformation u = this.GetAuthUserData();
            Console.WriteLine($"ProjectsController::GetProjects UserId: {u.UserId}");
            return await this.service.GetMembersOfProject(u.GitlabToken, id);
        }

        [HttpGet("{id}/labels")]
        public async Task<IEnumerable<LabelDTO>> GetLabels(int id)
        {
            UserInformation u = this.GetAuthUserData();
            Console.WriteLine($"ProjectsController::GetProjects UserId: {u.UserId}");
            return await this.service.GetLabelsOfProject(u.GitlabToken, id);
        }
    }
}
