﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Controllers
{
    [Authorize]
    [Route("projects")]
    [ApiController]
    public class IssuesController : ControllerBase
    {

        private IIssuesService _projectService;
        private IGitlabService _gitlabService;

        public IssuesController(IIssuesService projectService, IGitlabService gitlabService)
        {
            this._projectService = projectService;
            this._gitlabService = gitlabService;
        }

        [HttpGet("{id}/issues")]
        public async Task<IEnumerable<IssueResource>> GetProjectIssues(int id)
        {
            Console.WriteLine("GetProjectIssues::ProjectsController");
            UserInformation u = this.GetAuthUserData();
            return await _projectService.GetProjectIssues(id, u);
        }

        [HttpGet("{projectId}/issues/{issueId}")]
        public async Task<IssueResource> GetSingleProjectIssue(int projectId, int issueId)
        {
            Console.WriteLine("GetSingleProjectIssue::ProjectsController");
            UserInformation u = this.GetAuthUserData();
            return await _projectService.GetSingleProjectIssue(projectId, issueId, u);
        }
    }
}