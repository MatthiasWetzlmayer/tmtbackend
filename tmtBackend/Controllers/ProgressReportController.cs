﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [Authorize]
    [ApiController]
    public class ProgressReportController : ControllerBase
    {
        private readonly IProgressReportService progressReportService;
        public ProgressReportController(IProgressReportService progressReportService)
        {
            this.progressReportService = progressReportService;
        }

        // GET api/<ProgressReportController>/5
        [HttpGet("/projects/{projectId}/progressreport/")]
        public async Task<List<IssueOfUserResource>> Get(int projectId)
        {
            Console.WriteLine($"ProgressReportController::GetProgressReport ProjectId: {projectId}");
            UserInformation u = this.GetAuthUserData();
            string token = u.GitlabToken;
            return await progressReportService.ConvertIssueToIssueUserResources(token, projectId, int.Parse(u.UserId));
        }

        // GET api/<ProgressReportController>/5
        [HttpGet("/projects/{projectId}/progressreport/users/{userId}")]
        public async Task<IssueOfUserResource> Get(int projectId, int userId)
        {
            Console.WriteLine($"ProgressReportController::GetProgressReport ProjectId: {projectId} UserId: {userId}");
            UserInformation u = this.GetAuthUserData();
            string token = u.GitlabToken;
            return await progressReportService.ConvertIssueToIssueUserResource(token, projectId, userId, int.Parse(u.UserId));
        }
    }
}
