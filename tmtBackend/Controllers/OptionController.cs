﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Dto;
using tmtBackend.Helpers;
using tmtBackendDb;

namespace tmtBackend.Controllers
{
    [Authorize]
    [Route("options")]
    [ApiController]
    public class OptionController : Controller
    {
        private readonly OptionService service;

        public OptionController(OptionService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<OptionDto> GetOption()
        {
            var userData = this.GetAuthUserData();
            return service.GetOptionsByUserId(int.Parse(userData.UserId))
                .Select(x => new OptionDto
                {
                    Id = x.Id,
                    Name = x.SettingsName,
                    Value = x.SettingsValue,
                });
        }

        [HttpPost]
        public OptionDto AddOption([FromBody] OptionDto option)
        {
            var userData = this.GetAuthUserData();

            var setting = new Settings
            {
                SettingsName = option.Name,
                SettingsValue = option.Value,
                UserId = int.Parse(userData.UserId),
            };

            var result = service.AddOptionWithUserId(setting);

            return new OptionDto
            {
                Id = result.Id,
                Name = result.SettingsName,
                Value = result.SettingsValue,
            };
        }

        [HttpPut("{id}")]
        public ActionResult<OptionDto> EditOption(int id, [FromBody] OptionDto option)
        {
            var userData = this.GetAuthUserData();
            var userId = int.Parse(userData.UserId);

            var setting = new Settings
            {
                SettingsName = option.Name,
                SettingsValue = option.Value,
                UserId = userId,
            };

            var result = service.EditOptionWithUserId(id, userId, setting);

            if (result == null) return Unauthorized();

            return new OptionDto
            {
                Id = result.Id,
                Name = result.SettingsName,
                Value = result.SettingsValue,
            };
        }
    }
}
