﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using tmtBackend.Helpers;
using tmtBackend.Interfaces;
using tmtBackend.Models;

namespace tmtBackend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectForecastController : ControllerBase
    {
        private IProjectForecastService service;
        public ProjectForecastController(IProjectForecastService service)
        {
            this.service = service;
        }
        [HttpGet("{id}/coordinates")]
        public async Task<IEnumerable<WorkReportDataDTO>> GetDiagramCoordinates(int id, [FromQuery] string time)
        {
            UserInformation u = this.GetAuthUserData();
            var filter = new FilterDTO
            {
                Labels = null,
                TimePeriod = time
            };
            return  await service.calculateYCoordinates(id, u, filter);
        }
    }
}
