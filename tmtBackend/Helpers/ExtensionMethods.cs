﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Helpers
{
    public static class ExtensionMethods
    {
        public static UserInformation GetAuthUserData(this ControllerBase controller)
        {
            var identity = controller.HttpContext.User.Identity as ClaimsIdentity;
            string uId = identity?.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? "-1";
            string gitToken = identity?.FindFirst(ClaimTypes.Authentication)?.Value ?? "-1";
            return new UserInformation
            {
                UserId = uId,
                GitlabToken = gitToken
            };
        }

        public static T CopyPropertiesFrom<T>(this T target, object source, string[] ignoreProperties = null)
        {
            if (ignoreProperties == null) ignoreProperties = new string[] { };
            var propsSource = source.GetType().GetProperties()
                .Where(x => x.CanRead && !ignoreProperties.Contains(x.Name));
            var propsTarget = target.GetType().GetProperties()
                .Where(x => x.CanWrite);

            propsTarget
                .Where(prop => propsSource.Any(x => x.Name == prop.Name))
                .ToList()
                .ForEach(prop =>
                {
                    var propSource = propsSource.Where(x => x.Name == prop.Name).First();
                    prop.SetValue(target, propSource.GetValue(source));
                });
            return target;
        }
    }
}
