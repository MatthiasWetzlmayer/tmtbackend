﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Helpers
{
    public static class IssuesExtensionMethods
    {

        public static GraphDataTimeStats GetTimestatsOfIssues(this List<GitlabIssue> issues)
        {
            GraphDataTimeStats data = new GraphDataTimeStats();

            foreach (var issue in issues)
            {
                if (issue.Closed_at != null && DateTime.Parse(issue.Closed_at) < DateTime.Now)
                {
                    data.TotalEstimateFinished += issue.Time_stats.Time_estimate;
                    data.TotalSpendFinished += issue.Time_stats.Total_time_spent;
                }

                data.TotalEstimate += issue.Time_stats.Time_estimate;
                data.TotalSpend += issue.Time_stats.Total_time_spent;
            }

            return data;
        }

        public static GraphDataTimeStats GetTimestatsOfIssues(this List<SmallIssueResource> issues)
        {
            GraphDataTimeStats data = new GraphDataTimeStats();

            foreach (var issue in issues)
            {

                if (issue.ClosedAt != null && DateTime.Parse(issue.ClosedAt) < DateTime.Now)
                {
                    data.TotalEstimateFinished += issue.EstimatedHours;
                    data.TotalSpendFinished += issue.SpentHours;
                }

                data.TotalEstimate += issue.EstimatedHours;
                data.TotalSpend += issue.SpentHours;
            }

            return data;
        }

        public static List<GitlabIssue> FilterIssues(this List<GitlabIssue> gitlabIssues, FilterDTO filter)
        {
            List<GitlabIssue> issues = new List<GitlabIssue>();

            foreach (var issue in gitlabIssues)
            {
                bool labelOk = true;
                bool timeOk = false;

                if (filter.Labels != null && filter.Labels.Count > 0)
                {

                    foreach (var label in filter.Labels)
                    {
                        if (!issue.Labels.Contains(label))
                        {
                            labelOk = false;
                        }
                    }
                }
                else
                    labelOk = true;

                var d = DateTime.Parse(issue.Created_at);

                if (filter.TimePeriod == "week")
                {
                    if (DateTime.Now.AddDays(-7) < d)
                        timeOk = true;
                }
                else if (filter.TimePeriod == "month")
                {
                    if (DateTime.Now.AddDays(-30) < d)
                        timeOk = true;
                }
                else
                    timeOk = true;

                if (timeOk && labelOk)
                    issues.Add(issue);
            }

            return issues;
        }
    }
}
