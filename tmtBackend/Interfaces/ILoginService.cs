﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface ILoginService
    {
        public Task<TokenResource> GetToken(LoginDTO login);
    }
}
