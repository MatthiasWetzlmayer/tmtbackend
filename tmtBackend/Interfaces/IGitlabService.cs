﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IGitlabService
    {
        public Task<int> GetAccessLevelOfUserForProject(string token, int projectId);
        public Task<int> GetUserIdByToken(string token);
        public Task<List<GitlabIssue>> GetIssues(string token, int projectId);
        public Task<List<GitlabProjects>> GetProjects(string token);
        public Task<string> Login(LoginDTO login);
        public Task<IEnumerable<GitlabMember>> GetMembersOfProject(string token, int projectId);
        public Task<IEnumerable<GitlabLabel>> GetLabelsOfProject(string token, int projectId);

    }
}
