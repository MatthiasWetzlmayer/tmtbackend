﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IIssuesService
    {
        public Task<IEnumerable<IssueResource>> GetProjectIssues(int id, UserInformation u);
        public Task<IssueResource> GetSingleProjectIssue(int projectId, int issueId, UserInformation u);
    }
}
