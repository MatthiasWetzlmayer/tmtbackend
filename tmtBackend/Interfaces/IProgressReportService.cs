﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IProgressReportService
    {
        public Task<List<IssueOfUserResource>> ConvertIssueToIssueUserResources(string token, int projectId, int userId);

        public Task<IssueOfUserResource> ConvertIssueToIssueUserResource(string token, int projectId, int userId, int myId);

    }
}
