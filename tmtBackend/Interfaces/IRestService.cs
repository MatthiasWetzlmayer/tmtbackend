﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tmtBackend.Interfaces
{
    public interface IRestService
    {
        public Task<T> Get<T>(string path, Dictionary<string, string> headers);
        public Task<T> Post<T, B>(string path, Dictionary<string, string> headers, B body);
        public Task<T> Put<T, B>(string path, Dictionary<string, string> headers, B body);
        public Task<T> Delete<T>(string path, Dictionary<string, string> headers);
    }
}
