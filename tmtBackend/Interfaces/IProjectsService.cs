﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IProjectsService
    {
        public Task<IEnumerable<Project>> GetProjects(string gitlabToken);

        public Task<IEnumerable<MemberDTO>> GetMembersOfProject(string gitlabToken, int projectId);

        public Task<IEnumerable<LabelDTO>> GetLabelsOfProject(string gitlabToken, int projectId);

    }
}
