using System.Collections.Generic;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IProjectForecastService
    {
        public Task<IEnumerable<WorkReportDataDTO>> calculateYCoordinates(int id, UserInformation u, FilterDTO filter);
    }
}