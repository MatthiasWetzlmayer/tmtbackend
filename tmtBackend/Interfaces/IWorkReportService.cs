﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmtBackend.Models;

namespace tmtBackend.Interfaces
{
    public interface IWorkReportService
    {
        public Task<IEnumerable<WorkReportDataDTO>> GetWorkreportForUsers(string token, int projectId, int userId, FilterDTO filter);
        public Task<IEnumerable<WorkReportDataDTO>> GetCompleteWorkreport(string token, int projectId, int userId, FilterDTO filter);

    }
}
