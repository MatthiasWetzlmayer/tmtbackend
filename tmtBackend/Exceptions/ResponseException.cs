using System;

namespace tmtBackend.Exceptions
{
    public class ResponseException : Exception
    {
        public int StatusCode { get; set; }
        public String Message { get; set; }
    }
}