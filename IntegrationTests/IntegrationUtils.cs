﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using tmtBackend.Models;
using Xunit.Sdk;

namespace IntegrationTests
{
    public class IntegrationUtils
    {
        private static readonly string URL_Login = "http://localhost:5000/Login";

        private static readonly string URL_Projects = "http://localhost:5000/Projects";
        private static readonly string URL_Issues = "http://localhost:5000/projects";
        private static readonly string URL_ProgressReport = "http://localhost:5000";
        static AppSettings appSettings = new AppSettings() { Secret = "MySpecialSecretKey" };
        static IOptions<AppSettings> options = Options.Create(appSettings);

        public static async Task<HttpClient> Authentication(string Username, string Password, HttpClient client)
        {
            var matchedToken = await GetToken(Username, Password, client);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", matchedToken);
            return client;
        }

        public static async Task<string> GetToken(string Username, string Password, HttpClient client)
        {
            var content = new StringContent(JsonConvert.SerializeObject(new LoginDTO { Username = Username, Password = Password }), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(URL_Login, content);
            response.EnsureSuccessStatusCode();
            return Regex.Match(await response.Content.ReadAsStringAsync(), "(?<=\"token\":\").*(?=\"})").Value;
        }

        public static async Task<string> GetProjects(string Username, string Password, HttpClient client)
        {
            client = await Authentication(Username, Password, client);
            var response = await client.GetAsync(URL_Projects);
            response.EnsureSuccessStatusCode();
            if (response.StatusCode == HttpStatusCode.NoContent) throw new NullException("Responsecode 204");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetProjectsIssusesForProject(string Username, string Password, string ProjectId, HttpClient client)
        {
            client = await Authentication(Username, Password, client);
            var response = await client.GetAsync($"{URL_Issues}/{ProjectId}/issues");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetIssue(string Username, string Password, string ProjectId, string IssueId, HttpClient client)
        {
            client = await Authentication(Username, Password, client);
            var response = await client.GetAsync($"{URL_Issues}/{ProjectId}/issues/{IssueId}");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetProgressReport(string Username, string Password, string ProjectId, HttpClient client)
        {
            client = await Authentication(Username, Password, client);
            var response = await client.GetAsync($"{URL_ProgressReport}/projects/{ProjectId}/progressreport");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetProgressReport(string Username, string Password, string ProjectId, string UserId, HttpClient client)
        {
            client = await Authentication(Username, Password, client);
            var response = await client.GetAsync($"{URL_ProgressReport}/projects/{ProjectId}/progressreport/users/{UserId}");
            return await response.Content.ReadAsStringAsync();
        }

        public static object FileReader(string path, bool trim)
        {
            var parts = path.Split("\\");
            var globalPath = "";
            foreach (var part in parts) globalPath = Path.Combine(globalPath, part);
            string expected = File.ReadAllText(globalPath);
            if (trim)
                return expected.ToLower().Trim();
            else
                return expected;
        }
    }
}