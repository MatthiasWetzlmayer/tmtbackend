﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Threading;
using tmtBackend;
using Xunit;
using Xunit.Sdk;

namespace IntegrationTests
{
    public class IssueControllerIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public IssueControllerIntegrationTests(WebApplicationFactory<Startup> factory) => _factory = factory;
        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "22357774", "76391552", @"Files\TestIssuesSingle_2.json")]
        [InlineData("TestUser011", "ILoveSYP123", "22357774", "76391430", @"Files\TestIssuesSingle_1.json")]
        public async void TestIssues_Single(string Username, string Password, string projectId, string IssuseId, string path)
        {
            Thread.Sleep(15000);
            var expected = IntegrationUtils.FileReader(path, true);
            var actual = (await IntegrationUtils.GetIssue(Username, Password, projectId, IssuseId, _factory.CreateClient())).ToLower().Trim();
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "22357774", @"Files\TestIssuesAll_2.json")]
        [InlineData("TestUser011", "ILoveSYP123", "22357774", @"Files\TestIssuesAll_1.json")]
        public async void TestIssues_All(string Username, string Password, string projectId, string path)
        {
            Thread.Sleep(15000);
            var expected = IntegrationUtils.FileReader(path, true);
            var actual = (await IntegrationUtils.GetProjectsIssusesForProject(Username, Password, projectId, _factory.CreateClient())).ToLower().Trim();

            Assert.Equal(expected, actual);
        }

        #region Error_Tests

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "76391552")]
        public async void ProjectIssuesError(string Username, string Password, string projectId)
        {
            Thread.Sleep(15000);
            var val = await IntegrationUtils.GetProjectsIssusesForProject(Username, Password, projectId, _factory.CreateClient());
            if (val == "" || val == null)
                Assert.Equal("", val);
            else
                await Assert.ThrowsAsync<NullException>(() => IntegrationUtils.GetProjectsIssusesForProject(Username, Password, projectId, _factory.CreateClient()));
        }

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "76391552", "22357774")]
        public async void PersonIssueError(string Username, string Password, string ProjectId, string IssuseId)
        {
            Thread.Sleep(15000);
            var val = await IntegrationUtils.GetIssue(Username, Password, ProjectId, IssuseId, _factory.CreateClient());
            if (val == "" || val == null)
                Assert.Equal("", val);
            else
                await Assert.ThrowsAsync<Exception>(() => IntegrationUtils.GetIssue(Username, Password, ProjectId, IssuseId, _factory.CreateClient()));

        }

        #endregion
    }
}
