﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using tmtBackend;
using Xunit;

namespace IntegrationTests
{
    public class OptionControllerIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public OptionControllerIntegrationTests(WebApplicationFactory<Startup> factory) => _factory = factory;

        /*
         *  ToDo: 
         *  Create OptionDTOs
         *  IntegrationUtils Methods
         *  Files for each Test (GetOptions_1.txt)
         */

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", @"Files\GetOption_1.txt")]
        [InlineData("TestUser011", "ILoveSYP123", @"Files\GetOption_2.txt")]
        public async void GetOptions(string username, string password, string path)
        {
            Thread.Sleep(15000);
            var expected = IntegrationUtils.FileReader(path, false).ToString();
            var actual = await IntegrationUtils.GetToken(username, password, _factory.CreateClient());
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", @"Files\GetOption_1.txt")]
        [InlineData("TestUser011", "ILoveSYP123", @"Files\GetOption_2.txt")]
        public async void AddOptions(string username, string password, string path)
        {
            Thread.Sleep(15000);
            var expected = IntegrationUtils.FileReader(path, false).ToString();
            var actual = await IntegrationUtils.GetToken(username, password, _factory.CreateClient());
            Assert.Equal(expected, actual);
        }

    }
}
