﻿using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading;
using tmtBackend;
using Xunit;

namespace IntegrationTests
{
    public class _IntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public _IntegrationTests(WebApplicationFactory<Startup> factory) => _factory = factory;
        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "22357774")]
        //[InlineData("seifriedsbergerm0123@gmail.com", "TestPasswort1234", "21289226")]
        public async void GetProgressReport_Project(string Username, string Password, string ProjectId)
        {
            Thread.Sleep(15000);
            var temp = await IntegrationUtils.GetProgressReport(Username, Password, ProjectId, _factory.CreateClient());
        }

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "22357774", "7436950")]
        public async void GetProgressReport_User(string Username, string Password, string ProjectId, string UserId)
        {
            Thread.Sleep(15000);
            var temp = await IntegrationUtils.GetProgressReport(Username, Password, ProjectId, UserId, _factory.CreateClient());
        }
    }
}
