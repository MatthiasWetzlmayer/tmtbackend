﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using tmtBackendDb;

namespace IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        public tmtContext db;

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceDescriptor = services.SingleOrDefault(descriptor => descriptor.ServiceType == typeof(DbContextOptions<tmtContext>));
                if(serviceDescriptor != null) services.Remove(serviceDescriptor);

                services.AddDbContext<tmtContext>(o => o.UseSqlServer("data source=(localdb)\\mssqllocaldb;attachdbfilename=C:\\Temp\\SypTest.mdf; integrated security=True;MultipleActiveResultSets=True"));
                
            }
         );
        }

    }
}
