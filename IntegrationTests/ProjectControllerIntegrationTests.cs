﻿using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading;
using tmtBackend;
using Xunit;

namespace IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public ProjectControllerIntegrationTests(WebApplicationFactory<Startup> factory) => _factory = factory;

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", @"Files\TestProjects_2.json")]
        [InlineData("TestUser011", "ILoveSYP123", @"Files\TestProjects_1.json")]
        public async void TestProjects(string username, string password, string path)
        {
            Thread.Sleep(15000);
            var expected = IntegrationUtils.FileReader(path, true);
            var actual = (await IntegrationUtils.GetProjects(username, password, _factory.CreateClient())).ToLower().Trim();
            Assert.Equal(expected, actual);
        }

        #region Error_Tests
        #endregion
    }
}
