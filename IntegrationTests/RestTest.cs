﻿using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using tmtBackend;
using Xunit;

namespace IntegrationTests
{
    public class RestTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public RestTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }                                                                               

        [Fact]
        public async void TestGetAsync()
        {
            //tmtBackendContext yeet = (tmtBackendContext) _factory.Services.GetService(typeof(tmtBackendContext));
            //yeet.Users.Add(new User()
            //{
            //    Name = "yeet",            DOES NOT WORK CUZ THERE IS NO PK ¯\_(ツ)_/¯
            //    Email = "yeet@gmail.com"
            //});
            //yeet.SaveChanges();

            var client = _factory.CreateClient();
            var response = await client.GetAsync("http://localhost:5000/RestTest/GetTest?id=1337");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();

            Assert.Equal("true", content);
        }

        [Fact]
        public async void TestPostAsync()
        {
            string str = "test";

            var content = new StringContent(
            JsonConvert.SerializeObject(str),
            Encoding.UTF8,
            "application/json");

            var client = _factory.CreateClient();
            var response = await client.PostAsync("http://localhost:5000/RestTest/PostTest", content);
            response.EnsureSuccessStatusCode();

            var contentres = await response.Content.ReadAsStringAsync();

            Assert.Equal("true", contentres);
        }

        [Fact]
        public async void TestPutAsync()
        {
            string str = "test";

            var content = new StringContent(
            JsonConvert.SerializeObject(str),
            Encoding.UTF8,
            "application/json");

            var client = _factory.CreateClient();
            var response = await client.PutAsync("http://localhost:5000/RestTest/PutTest", content);
            response.EnsureSuccessStatusCode();

            var contentres = await response.Content.ReadAsStringAsync();

            Assert.Equal("true", contentres);
        }

        [Fact]
        public async void TestDeleteAsync()
        {
            var client = _factory.CreateClient();
            var response = await client.DeleteAsync("http://localhost:5000/RestTest/DeleteTest?id=1337");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();

            Assert.Equal("true", content);
        }

        
    }
}
