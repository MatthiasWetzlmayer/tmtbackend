﻿using System;
using System.Collections.Generic;

namespace tmtBackendDb
{
    public partial class Userstorys
    {
        public int Id { get; set; }
        public int StoryId { get; set; }
        public string EstimatedTime { get; set; }
        public string SpentTime { get; set; }
    }
}
