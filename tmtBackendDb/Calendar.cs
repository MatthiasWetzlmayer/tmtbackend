﻿using System;
using System.Collections.Generic;

namespace tmtBackendDb
{
    public partial class Calendar
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
    }
}
