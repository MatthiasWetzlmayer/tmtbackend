﻿using System;
using System.Collections.Generic;

namespace tmtBackendDb
{
    public partial class Settings
    {
        public int Id { get; set; }
        public string SettingsName { get; set; }
        public string SettingsValue { get; set; }
        public int UserId { get; set; }
    }
}
