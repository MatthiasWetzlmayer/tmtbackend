﻿using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace UnitTests
{
    public class Utils
    {
        public static string GetGitlabTokenFromToken(string token)
        {

            var claimIdentity = ValidateToken(token).Identity as ClaimsIdentity;

            return claimIdentity?.FindFirst(ClaimTypes.Authentication)?.Value ?? "-1";
        }

        private static ClaimsPrincipal ValidateToken(string jwtToken)
        {
            IdentityModelEventSource.ShowPII = true;

            SecurityToken validatedToken;
            TokenValidationParameters validationParameters;

            // Copy the Validation Parameters from the startup.cs here
            validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MySpecialSecretKey")),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(jwtToken, validationParameters, out validatedToken);

            return principal;
        }

        public static string GetPath(string path)
        {
            var parts = path.Split("\\");
            var globalPath = "";

            foreach (var part in parts)
            {
                globalPath = Path.Combine(globalPath, part);
            }

            return globalPath;
        }

    }
}
