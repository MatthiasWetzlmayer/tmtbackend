﻿using Microsoft.Extensions.Options;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Testing;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;
using tmtBackendDb;
using Xunit;

namespace UnitTests
{
    public class LoginServiceTest
    {
        private static IConfiguration conf = UnitTestUtils.MockConfig();
        static AppSettings appSettings = new AppSettings() { Secret = "MySpecialSecretKey" };
        static IOptions<AppSettings> options = Options.Create(appSettings);
        LoginService login = new LoginService(new GitlabService(new RestService(), conf), options, new tmtContext());

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123")]
        [InlineData("TestUser011", "ILoveSYP123")]
        public async void GetTokenNotNullTest_01(string username, string password)
        {
            var loginDto = new LoginDTO
            {
                Password = password,
                Username = username,
            };
            string actual = (await login.GetToken(loginDto)).Token;
            Assert.NotNull(actual);
        }

        [Theory]
        [InlineData("TestUser010", "ILoveSYP123", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9", 360)]
        [InlineData("TestUser011", "ILoveSYP123", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9", 360)]

        public async void GetTokenValidTest_01(string username, string password, string expectedSubstring, int expectedLength)
        {
            var loginDto = new LoginDTO
            {
                Password = password,
                Username = username,
            };
            string actual = (await login.GetToken(loginDto)).Token;
            Assert.Contains(expectedSubstring, actual);
            Assert.Equal(expectedLength, actual.Length);
        }

        [Theory]
        [InlineData("TestUser010", "s")]
        [InlineData("TestUser011", "s")]

        public async void GetTokenInvalidTest_01(string username, string password)
        {
            var loginDto = new LoginDTO
            {
                Password = password,
                Username = username,
            };
            await Assert.ThrowsAnyAsync<System.ArgumentNullException>(async () => await login.GetToken(loginDto));
        }
    }
}