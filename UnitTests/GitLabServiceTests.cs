using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Testing;
using tmtBackend.Interfaces;
using tmtBackend.Services;
using Xunit;

namespace UnitTests
{
    public class GitLabServiceTests
    {
        private static IConfiguration conf = UnitTestUtils.MockConfig();
        //ProjectId: 21919302 <== Yeet
        //ProjectId: 22357774 <== Honks
        //ProjectId: 22359843 <== PublicProject

        //TestUser010 ==> "e4f0cc56221423d4b29faad59cb7ac1b3967a54747d9ab4fec47c7efc55858d0"
        //TestUser011 ==> "54025aa48e2f121d6144b4b36e8b1c081c508e2cfaa9eb4a017f95dcd4513c12"
        //TestUser020 ==> 
        private static IRestService rest = new RestService();
        GetUserStuff gUS = new GetUserStuff(rest);
        GitlabService gs = new GitlabService(rest, conf);



        [Theory]
        [InlineData(22663091, 40, "020ca0336998bb3035937909d0decd65df3c36bcaff780c99bfa5f1a79c3915d")] // TestOwner
        [InlineData(22359843, 40, "020ca0336998bb3035937909d0decd65df3c36bcaff780c99bfa5f1a79c3915d")] // PublicProject
        [InlineData(22357774, 50, "0bf058e50c3a7d1c285ff7b51a52a2f7e91de11d851975e1db028c1919ec2548")] // Learn GitLab
        public async void GetAccessLevelOfUserForProjectTest_01(int projectId, int expected, string token)
        {
            Thread.Sleep(1000);
            var alevel = await gs.GetAccessLevelOfUserForProject(token, projectId);
            Assert.Equal(expected, alevel);
        }

        [Theory]
        [InlineData(13377331, "TestUser010", 40)]
        [InlineData(22359843, "YeetUser010", 40)]
        [InlineData(21919303, "TestUser010", 1337)]
        [InlineData(null, "TestUser010", 1337)]
        [InlineData(22359843, null, 1337)]
        [InlineData(21919303, "TestUser010", null)]
        [InlineData(-22663091, "TestUser010", 40)]
        [InlineData(22663091, "TestUser010", -40)]
        public void EXCEPTION_GetAccessLevelOfUserForProjectTest(int projectId, string username, int expected)
        {
            Thread.Sleep(500);
            Assert.ThrowsAnyAsync<Exception>(async () => await GetAccessLevelMethod(projectId, username, expected));
        }

        private async Task<string> GetAccessLevelMethod(int projectId, string username, int excpected)
        {
            foreach (var user in GetUserStuff.users)
            {
                Thread.Sleep(1000);
                user.UserId = await gs.GetUserIdByToken(gUS.GetUserToken(username, user.Password).ToString());
                Thread.Sleep(1000);
                user.UserToken = await gUS.GetUserToken(username, user.Password);
                Thread.Sleep(1000);
                user.AccessLevel = await gs.GetAccessLevelOfUserForProject(user.UserToken, projectId);
            }

            return "";
        }

        [Theory]
        [InlineData(22357805, "54025aa48e2f121d6144b4b36e8b1c081c508e2cfaa9eb4a017f95dcd4513c12", ".\\Files\\GitLabServiceTests\\issues1.json")] // MyAwesomeSUCC
        [InlineData(21919302, "e4f0cc56221423d4b29faad59cb7ac1b3967a54747d9ab4fec47c7efc55858d0", ".\\Files\\GitLabServiceTests\\issues2.json")] // Yeet

        public async void GetIssuesTest_01(int projectId, string token, string path)
        {
            gUS.FillUsers();
            Thread.Sleep(1000);
            var globalPath = Utils.GetPath(path);
            string expected = File.ReadAllText(globalPath);
            var actual = System.Text.Json.JsonSerializer.Serialize(await gs.GetIssues(token, projectId));
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1337)]
        [InlineData(-1337)]
        [InlineData(null)]
        public void EXCEPTION_GetIssuesTest(int projectId)
        {
            Thread.Sleep(1000);
            Assert.ThrowsAnyAsync<Exception>(async () => await GetIssuesMethod(projectId));
        }

        private async Task<string> GetIssuesMethod(int projectId)
        {
            foreach (var user in GetUserStuff.users)
            {
                Thread.Sleep(1000);
                user.Issues = await gs.GetIssues(gUS.GetUserToken(user.Username, user.Password).ToString(), projectId);
            }

            return "";
        }

        [Theory]
        [InlineData(7436950)] //TestUser010
        [InlineData(7611342)] //TestUser011

        public async void GetUserIdByTokenTest_01(int expected)
        {
            foreach (var user in GetUserStuff.users)
            {
                Thread.Sleep(1000);
                var a = (await gUS.GetUserToken(user.Username, user.Password));
                Thread.Sleep(1000);
                user.UserId = await gs.GetUserIdByToken(a);
                if (expected == user.UserId)
                    Assert.Equal(expected, user.UserId);
            }
        }

        [Theory]
        [InlineData(1337)]
        [InlineData(-1337)]
        [InlineData(null)]

        public void EXCEPTION_GetUserIdByToken(int expected)
        {
            Thread.Sleep(1000);
            Assert.ThrowsAnyAsync<Exception>(async () => await GetUserIdByTokenMethod(expected));
        }

        private async Task<string> GetUserIdByTokenMethod(int expected)
        {
            foreach (var user in GetUserStuff.users)
            {
                Thread.Sleep(1000);
                user.UserId = await gs.GetUserIdByToken((await gUS.GetUserToken(user.Username, user.Password)));
                if (expected != user.UserId) throw new ArgumentException("The numbers are not the same - AS INTENDED");
            }

            return "";
        }

        [Theory]
        [InlineData(-2)]
        [InlineData(-3)]
        public async void GetUserIdByTokenTest_02(int expected)
        {
            foreach (var user in GetUserStuff.users)
            {
                Thread.Sleep(1000);
                var a = await gUS.GetUserToken(user.Username, user.Password);
                Thread.Sleep(1000);
                user.UserId = await gs.GetUserIdByToken(a);
                Assert.NotEqual(expected, user.UserId);
            }
        }

        [Theory]
        [InlineData("e4f0cc56221423d4b29faad59cb7ac1b3967a54747d9ab4fec47c7efc55858d0", ".\\Files\\GitLabServiceTests\\projects1.json")]
        [InlineData("54025aa48e2f121d6144b4b36e8b1c081c508e2cfaa9eb4a017f95dcd4513c12", ".\\Files\\GitLabServiceTests\\projects2.json")]
        public void GetProjectsTest_01(string token, string path)
        {
            gUS.FillUsers();
            var globalPath = Utils.GetPath(path);
            string expected = File.ReadAllText(globalPath);
            var actual = System.Text.Json.JsonSerializer.Serialize(gs.GetProjects(token));
            Thread.Sleep(1000);
            var projects = gs.GetProjects(token);
            Assert.Equal(projects, projects);
        }
    }
}
