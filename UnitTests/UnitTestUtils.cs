using System.IO;
using Microsoft.Extensions.Configuration;

namespace UnitTests
{
    public static class UnitTestUtils
    {
        public static IConfiguration MockConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}