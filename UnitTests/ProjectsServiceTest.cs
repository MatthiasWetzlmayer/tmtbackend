﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Testing;
using tmtBackend.Models;
using tmtBackend.Services;
using tmtBackendDb;
using Xunit;
using Newtonsoft.Json;

namespace UnitTests
{
    public class ProjectsServiceTest
    {
        private static IConfiguration conf = UnitTestUtils.MockConfig();
        static AppSettings appSettings = new AppSettings() { Secret = "MySpecialSecretKey" };
        static IOptions<AppSettings> options = Options.Create(appSettings);
        LoginService login = new LoginService(new GitlabService(new RestService(), conf), options, new tmtContext());
        ProjectsService projects = new ProjectsService(new GitlabService(new RestService(), conf));

        [Theory]
        [InlineData("020ca0336998bb3035937909d0decd65df3c36bcaff780c99bfa5f1a79c3915d")]
        [InlineData("0bf058e50c3a7d1c285ff7b51a52a2f7e91de11d851975e1db028c1919ec2548")]
        public async void GetAllProjects(string token)
        {
            string[] Projects = { "TestOwner", "PP", "Learn GitLab", "Honks", "Yeet", "MyAwesomeSUCC" };

            var k = await projects.GetProjects(token);
            foreach (var project in k)
            {
                Assert.True(Projects.Contains(project.Name));
            }
        }

        [Theory]
        [InlineData("020ca0336998bb3035937909d0decd65df3c36bcaff780c99bfa5f1a79c3915d", @".\Files\ProjectsServiceTest\projects.json")]
        [InlineData("0bf058e50c3a7d1c285ff7b51a52a2f7e91de11d851975e1db028c1919ec2548", @".\Files\ProjectsServiceTest\projects2.json")]
        public async void GetAllProjectsJson(string token, string path)
        {
            var actual = await projects.GetProjects(token);
            var globalPath = Utils.GetPath(path);
            string expected = File.ReadAllText(globalPath);
            string actual1 = JsonConvert.SerializeObject(actual);
            Assert.Equal(expected.ToLower().Trim(), actual1.ToLower().Trim());

            Assert.True(true);
        }

        [Theory]
        [InlineData("ThisShouldNotWork", "ThisShouldNotWork")]
        [InlineData("1337", "7331")]
        public async void EXCEPTION_GetAllProjects(string username, string password)
        {
            LoginDTO loginDto = new LoginDTO
            {
                Password = password,
                Username = username,
            };

            await Assert.ThrowsAnyAsync<Exception>(async () => await login.GetToken(loginDto));
        }
    }
}