﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Moq;
using Testing;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;
using Xunit;

namespace UnitTests
{
    public class IssueServiceTest
    {
        private static IConfiguration conf = UnitTestUtils.MockConfig();
        static AppSettings appSettings = new AppSettings() { Secret = "MySpecialSecretKey" };
        static IOptions<AppSettings> options = Options.Create(appSettings);
        IssuesService _issuesService = new IssuesService(new GitlabService(new RestService(), conf));

        private static IRestService rest = new RestService();

        const string gitlabToken = "e4f0cc56221423d4b29faad59cb7ac1b3967a54747d9ab4fec47c7efc55858d0";
        const string testUser010UserId = "7436950";
        const string TestUser011UserId = "";
        
        [Theory]
        [InlineData(21919303, gitlabToken, testUser010UserId)]
        [InlineData(22357774, gitlabToken, testUser010UserId)]
        [InlineData(22357775, gitlabToken, testUser010UserId)]
        public async void GetProjectIssuesNotNullTest(int id, string gitlabToken, string userId)
        {
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            IEnumerable<SmallIssueResource> actual = await _issuesService.GetProjectIssues(id, u);
            Assert.NotNull(actual);
        }

        [Theory]
        [InlineData(21919303, gitlabToken, testUser010UserId, @".\Files\IssueServiceTest\ProjectIssues\expected1.json")]
        [InlineData(22357774, gitlabToken, testUser010UserId, @".\Files\IssueServiceTest\ProjectIssues\expected2.json")]
        [InlineData(22357775, gitlabToken, testUser010UserId, @".\Files\IssueServiceTest\ProjectIssues\expected3.json")]
        public async void GetProjectIssuesValidTest_01(int id, string gitlabToken, string userId,  string path)
        {
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            List<SmallIssueResource> actual = (await _issuesService.GetProjectIssues(id, u)).ToList();

            var parts = path.Split("\\");
            var globalPath = "";

            foreach (var part in parts)
            {
                globalPath = Path.Combine(globalPath, part);
            }
            
            string expected = File.ReadAllText(globalPath);
            string actual1 = JsonConvert.SerializeObject(actual);
            Assert.Equal(expected.ToLower().Trim(), actual1.ToLower().Trim());
            //Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(22357774, "asdfa", testUser010UserId)]
        [InlineData(33, gitlabToken, testUser010UserId)]
        public async void GetProjectIssuesInvalidTest_01(int id, string gitlabToken, string userId)
        {
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            Assert.Null(await _issuesService.GetProjectIssues(id, u));
        }

        [Theory]
        [InlineData(21919303, 73075960, gitlabToken, testUser010UserId)]
        [InlineData(22357774, 76391552, gitlabToken, testUser010UserId)]
        [InlineData(22357775, 74146297, gitlabToken, testUser010UserId)]

        public async void GetSingleProjectIssueNotNullTest(int id, int issueId, string gitlabToken, string userId)
        {
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            IssueResource actual = await _issuesService.GetSingleProjectIssue(id, issueId, u);
            Assert.NotNull(actual);
        }

        [Theory]
        [InlineData(21919303, 73075960, gitlabToken, testUser010UserId, @"Files\IssueServiceTest\SingleProjectIssue\expected1.json")]
        [InlineData(22357774, 76391552, gitlabToken, testUser010UserId, @"Files\IssueServiceTest\SingleProjectIssue\expected2.json")]
        [InlineData(22357775, 74146297, gitlabToken, testUser010UserId, @"Files\IssueServiceTest\SingleProjectIssue\expected3.json")]
        public async void GetSingleProjectIssueValidTest_01(int id, int issueId, string gitlabToken, string userId, string path)
        {
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            IssueResource actual = await _issuesService.GetSingleProjectIssue(id, issueId, u);
            
            var parts = path.Split("\\");
            var globalPath = "";

            foreach (var part in parts)
            {
                globalPath = Path.Combine(globalPath, part);
            }
            
            string expected = File.ReadAllText(globalPath);
            string actual1 = JsonConvert.SerializeObject(actual);
            Assert.Equal(expected.ToLower().Trim(), actual1.ToLower().Trim());
            //Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(22357774, 76391552, "asdfa", testUser010UserId)]
        [InlineData(33, 74146297, gitlabToken, testUser010UserId)]
        public async void GetSingleProjectIssueInvalidTest_01(int id, int issueId, string gitlabToken, string userId)
        { 
            Thread.Sleep(1000);
            UserInformation u = new UserInformation
            {
                GitlabToken = gitlabToken,
                UserId = userId,
            };
            IssueResource actual = await _issuesService.GetSingleProjectIssue(id, issueId, u);
            Thread.Sleep(1000);
            Assert.NotNull(_issuesService.GetProjectIssues(id, u));
        }

    }
}
