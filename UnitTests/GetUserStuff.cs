﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Testing;
using tmtBackend.Interfaces;
using tmtBackend.Models;
using tmtBackend.Services;
using tmtBackendDb;
using Xunit;

namespace UnitTests
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public int UserId { get; set; }

        public string UserToken { get; set; }

        public List<GitlabIssue> Issues { get; set; }

        public int AccessLevel { get; set; }

        public object Projects { get; set; }
    }

    public class GetUserStuff
    {
        private static IConfiguration conf = MockConfig();
        private static readonly HttpClient client = new HttpClient();
        public static List<User> users = new List<User>();

        static AppSettings appSettings = new AppSettings() { Secret = "MySpecialSecretKey" };
        IOptions<AppSettings> options = Options.Create(appSettings);
        LoginService login;
        GitlabService gs;
        public GetUserStuff(IRestService rest)
        {
            login = new LoginService(new GitlabService(new RestService(), conf), options, new tmtContext());
            gs = new GitlabService(rest, conf);
        }
        
        private static IConfiguration MockConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        public async Task<string> GetUserToken(string username, string password)
        {
                FillUsers();
                foreach (var user in users)
                {
                    if (user.Username.Equals(username))
                    {
                        user.UserToken = await gs.Login(new LoginDTO
                        {
                            Username = username,
                            Password = password,
                        });
                        return user.UserToken;
                    }
                }
                return "-1";
            
        }

        public void FillUsers()
        {
            if (users.Count == 0)
            {
                users.Add(new User { Username = "TestUser010", Password = "ILoveSYP123", UserToken = "e4f0cc56221423d4b29faad59cb7ac1b3967a54747d9ab4fec47c7efc55858d0" });
                users.Add(new User { Username = "TestUser011", Password = "ILoveSYP123", UserToken = "54025aa48e2f121d6144b4b36e8b1c081c508e2cfaa9eb4a017f95dcd4513c12" });
                users.Add(new User { Username = "TestUser020", Password = "TestUser020", UserToken = "2a4de77ea34a3484c7f1e73333c158472999651bae19cfef100551fa570986a4" });
            }
        }

    }
}